<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component\Form;

use DataExplorer\Component\Component;
use DataExplorer\Component\Text\P;
use DataExplorer\Component\Text\A;
use DataExplorer\Component\Text\Label;
use DataExplorer\Component\Form\Input\Input;
use DataExplorer\Component\Form\Input\Submit;
use DataExplorer\DataExplorer;

/**
* Class which represents a html form.
*
* This class is used to model a <code>form</code> (HTML).
*
* @package DataExplorer\Component\Form
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
*/
class Form extends Component {

    /**
    * @var string Action's form
    */
    protected $action;

    /**
    * @var string Method's form
    */
    protected $method;

    /**
    * Constructor
    *
    * @param string $method Method's form
    * @param string $action Action's form
    */
    public function __construct(string $method, string $action = '') {
        parent::__construct();

        $this->method = $method;
        $this->action = $action;
    }

    /**
    * Adds a child node (not usable in this class)
    *
    * @param Component $node the child
    * @param string $key The key of the child inside the array can be null
    *
    * @return void
    * @throws Exception
    */
    public function addNode(Component $node, string $key = null) : void {
        throw new \Exception("addNode is not available with ".__CLASS__.".\n
                    Use addInput method", 1);
    }

    /**
    * Adds input inside the form.
    * This method will create a paragraph with, inside, a label and the input.
    *
    * @param Input $input Input to add
    * @param DataExplorer $de DataExplorer instance.
    *                It's necessary to know if the default CSS is enabled.
    *
    * @return void
    */
    public function addInput(Input $input, DataExplorer $de) : void{
        $p    = new P();
        $name = $input->name();

        if(!($input instanceof Submit)) {
            $p->addNode(new Label(\ucfirst($name), $name), 'label');
        } else {
            if($de->default_css_enabled())
                $input->addClass("btn--primary");
        }

        $p->addNode($input, 'input');

        $this->nodes[$name] = $p;
    }

    /**
    * Gets the method's form
    *
    * @return string Method's form
    */
    public function method() : string {
        return $this->method;
    }

    /**
    * Gets the action's form
    *
    * @return string action's form
    */
    public function action() : string {
        return $this->action;
    }

    /**
    * Gets the label associated to an input
    *
    * @param string $name Input name
    *
    * @return Label The label associated to the input
    */
    public function label(string $name) : Label {
        return $this->nodes[$name]->getNode('label');
    }

    /**
    * Gets an input by his name
    *
    * @param string $name Input name
    *
    * @return Input The input
    */
    public function input(string $name) : Input {
        return $this->nodes[$name]->getNode('input');
    }

    /**
    * Gets all inputs inside form, without submit input
    *
    * @return array Input array
    */
    public function inputs() : array {
        $nodes = $this->nodes;
        unset($nodes['submit']);
        return $nodes;
    }

    /**
    * Update an input
    *
    * @param string $name Input name
    * @param Input $in The new input
    *
    * @return void
    */
    public function setInput(string $name, Input $in) : void {
        $this->nodes[$name]->addNode($in, 'input');
    }

    /**
    * Change the method's form
    *
    * @param string $method The method's form (GET | POST)
    *
    * @return void
    */
    public function setMethod(string $method) : void {
        $this->method = $method;
    }

    /**
    * Change the action's form
    *
    * @param string $action The action's form
    *
    * @return void
    */
    public function setAction(string $action) : void {
        $this->action = $action;
    }

    /**
    * Sets the reset link.
    *
    * @param string $link The reset link
    * @param DataExplorer $de DataExplorer instance.
    *                It's necessary to know if the default CSS is enabled.
    *
    * @return void
    */
    public function setReset(string $link, DataExplorer $de) : void {
        $a = new A("Reset", $link);
        $a->addClass("btn");

        $this->nodes['submit']->addNode($a,'reset');

        if($de->default_css_enabled())
            $this->nodes['submit']->addClasses(['col-all','txtright']);
    }

    public function render() : string{
        $html = "<form action='{$this->action}' method='{$this->method}'
                id='{$this->id}' class='".\implode(' ',$this->class)."' {$this->html_attrs()}>"
                    .PHP_EOL;

        foreach($this->nodes as $c){
            $html .= $c->render().PHP_EOL;
        }

        $html .= "</form>";

        return $html;
    }
}

 ?>
