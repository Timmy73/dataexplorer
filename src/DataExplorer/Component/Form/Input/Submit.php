<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component\Form\Input;

/**
* Class which represents the submit input.
*
* This class is used to model the <code>submit</code> input (HTML).
*
* @package DataExplorer\Component\Form\Input
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @used-by DataExplorer\Component\Form\Form
* @version 0.1
*/
class Submit extends Hidden {

    /**
    * Constructor
    *
    * @param string $name The input name
    * @param string $value The input value 
    */
    public function __construct(string $name, string $value){
        parent::__construct($name,$value);

        $this->type = "submit";
    }
}

 ?>
