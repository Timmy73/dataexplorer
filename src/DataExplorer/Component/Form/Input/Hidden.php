<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component\Form\Input;

/**
* Class which represents the hidden input.
*
* This class is used to model the <code>hidden</code> input (HTML).
*
* @package DataExplorer\Component\Form\Input
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @used-by DataExplorer\Component\Form\Form
* @version 0.1
*/
class Hidden extends Input {

    /**
    * Constructor
    *
    * @param string $name Input name
    * @param string $value Input value
    */
    public function __construct(string $name, string $value){
        parent::__construct($name,'');

        $this->type = "hidden";
        $this->options['value'] = $value;
    }
}

 ?>
