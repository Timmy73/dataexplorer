<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component\Form\Input;

use DataExplorer\Component\Component;

/**
* Class which represents the input.
*
* This class represents in a general way the different html inputs.
*
* @package DataExplorer\Component\Form\Input
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @used-by DataExplorer\Component\Form\Form
* @version 0.1
*/
class Input extends Component {

    /**
    * @var string Type of input
    */
    protected $type;

    /**
    * @var string Input name
    */
    protected $name;

    /**
    * @var string Placeholder of input
    */
    protected $placeholder;

    /**
    * Constructor
    *
    * @param string $name Input name
    * @param string $placeholder Placeholder of input
    */
    public function __construct(string $name, string $placeholder = ""){
        parent::__construct();

        $this->type        = "text";
        $this->name        = \strtolower($name);
        $this->placeholder = $placeholder;
        $this->options     = [];
    }

    /**
    * Gets all HTML attributes of input
    *
    * @return array All HTML attributes of input
    */
    public function options() : array {
        return $this->options;
    }

    /**
    * Sets the input name
    *
    * @param string $name Input name
    *
    * @return void
    */
    public function setName(string $name) : void {
        $this->$name = $name;
    }

    /**
    * Gets the input name
    *
    * @return string The input name
    */
    public function name() : string {
        return $this->name;
    }

    /**
    * Sets the placeholder
    *
    * @param string $placeholder Placeholder
    *
    * @return void
    */
    public function setPlaceholder(string $placeholder) : void {
        $this->placeholder = $placeholder;
    }

    public function render() : string {
        return "<input type='{$this->type}' name='{$this->name}' id='{$this->id}'
                class='".\implode(' ',$this->class)."' placeholder='{$this->placeholder}'
                {$this->html_attrs()} />";
    }

}

 ?>
