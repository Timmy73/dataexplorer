<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component\Form\Input;

/**
* Class which represents the number input.
*
* This class is used to model the <code>number</code> input (HTML).
*
* @package DataExplorer\Component\Form\Input
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @used-by DataExplorer\Component\Form\Form
* @version 0.1
*/
class Number extends Input {

    /**
    * Constructor
    *
    * @param string $name The input name
    * @param string $placeholder The placeholder of input
    */
    public function __construct(string $name, string $placeholder = ""){
        parent::__construct($name,$placeholder);

        $this->type = "number";
    }
}

 ?>
