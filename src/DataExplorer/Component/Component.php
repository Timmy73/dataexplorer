<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component;

/**
* Class which globaly represents any html component.
*
* @abstract
* @package DataExplorer\Component
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
*/
abstract class Component {

    /**
    * @var array containing the CSS classes
    */
    protected $class;
    /**
    * @var string CSS Id of the element
    */
    protected $id;
    /**
    * @var array Component array which contains the child of the current node
    */
    protected $nodes;
    /**
    * @var array Array containing different html attributes
    */
    protected $options;

    /**
    * Constructor
    */
    public function __construct(){
        $this->class   = [];
        $this->nodes   = [];
        $this->options = [];
    }

    /**
    * Adds a CSS class
    *
    * @param string $class The CSS class
    *
    * @return void
    */
    public function addClass(string $class) : void {
        $this->class[] = $class;
    }

    /**
    * Adds a set of CSS classes
    *
    * @param array $classes The set of classes
    *
    * @return void
    */
    public function addClasses(array $classes) : void{
        $this->class = \array_merge($this->class, $classes);
    }

    /**
    * Sets the CSS id of the element
    *
    * @param string $id The id
    *
    * @return void
    */
    public function setId(string $id) : void {
        $this->id = \strtolower($id);
    }

    /**
    * Adds a child node
    *
    * @param Component $node the child
    * @param string $key The key of the child inside the array can be null
    *
    * @return void
    */
    public function addNode(Component $node, string $key = null) : void {
        if(isset($key) && !empty($key))
            $this->nodes[$key] = $node;
        else
            $this->nodes[] = $node;
    }

    /**
    * Adds a set of HTML attributes
    *
    * @param array $options The attributes array
    *
    * @return void
    */
    public function setOptions(array $options) : void {
        $this->options = \array_merge($this->options, $options);
    }

    /**
    * Adds a HTML attribute
    *
    * @param string $name Attribute name
    * @param string $value Attribute value
    *
    * @return void
    */
    public function addOption(string $name, string $value) : void {
        $this->options[$name] = $value;
    }

    /**
    * Gets a child of this element with a key
    *
    * @param string $k The key of the child to get
    *
    * @return Component The element corresponding to the key <code>$k</code>
    */
    public function getNode(string $k) : Component {
        return $this->nodes[$k];
    }

    /**
    * Removes all child nodes
    *
    * @return void
    */
    public function clearNodes() : void {
        $this->nodes = [];
    }

    /**
    * Generate the HTML code of this component
    *
    * @return string The HTML code
    */
    abstract public function render()  : string;

    /**
    * Returns the attributes as HTML code
    *
    * @return string The HTML code
    */
    protected function html_attrs() : string {
        $html_options = "";

        if(\is_array($this->options)){
            foreach($this->options as $k => $v){
                $html_options .= "$k='$v' ";
            }
        }

        return $html_options;
    }
}
