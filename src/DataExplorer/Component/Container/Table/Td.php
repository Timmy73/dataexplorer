<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component\Container\Table;

/**
* Class which represents a html td.
*
* This class is used to model a <code>td</code> (HTML).
*
* @package DataExplorer\Component\Container\Table
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
*/
class Td extends Tr{

    /**
    * @var string Text of td
    */
    protected $txt;

    /**
    * Constructor
    *
    * @param string $txt Text of td
    */
    public function __construct(string $txt = ""){
        parent::__construct();

        $this->txt = $txt;
    }

    /**
    * setText Sets the text of td
    *
    * @param string $txt Text of td
    *
    * @return void
    */
    public function setText(string $txt) : void {
        $this->txt = $txt;
    }

    /**
    * txt Gets text of td
    *
    * @return string Text of td
    */
    public function txt() : string {
        return $this->txt;
    }

    public function render() : string{
        $html = "<td id='{$this->id}' class='".\implode(' ',$this->class)."'
                    {$this->html_attrs()}>"
                    .PHP_EOL;

        $html .= $this->txt . PHP_EOL;
        foreach($this->nodes as $c){
            $html .= $c->render().PHP_EOL;
        }

        $html .= "</td>";

        return $html;
    }
}



 ?>
