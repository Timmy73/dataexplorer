<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component\Container\Table;

/**
* Class which represents a html tr.
*
* This class is used to model a <code>tr</code> (HTML).
*
* @package DataExplorer\Component\Container\Table
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
*/
class Tr extends Table{

    public function __construct(){
        $this->class = [];
        $this->nodes = [];
    }

    public function render() : string{
        $html = "<tr id='{$this->id}' class='".\implode(' ',$this->class)."' {$this->html_attrs()}>"
                    .PHP_EOL;

        foreach($this->nodes as $c){
            $html .= $c->render().PHP_EOL;
        }
        $html .= "</tr>";

        return $html;
    }
}



 ?>
