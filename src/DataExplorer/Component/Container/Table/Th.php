<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component\Container\Table;

/**
* Class which represents a html th.
*
* This class is used to model a <code>th</code> (HTML).
*
* @package DataExplorer\Component\Container\Table
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
*/
class Th extends Td{

    public function render() : string{
        $html = "<th id='{$this->id}' class='".\implode(' ',$this->class)."' {$this->html_attrs()}>";

        $html .= \ucfirst($this->txt);

        $html .= "</th>";

        return $html;
    }
}



 ?>
