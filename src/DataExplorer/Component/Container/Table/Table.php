<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component\Container\Table;

use DataExplorer\Component\Container\Div;
use DataExplorer\Component\Text\Caption;
use DataExplorer\DataExplorer;

/**
* Class which represents a html table.
*
* This class is used to model a <code>table</code> (HTML).
* When we create a new instance of this class, we give the data to put in the table.
* This class will generate automatically the table with a header an a body.
* Column names are the key of the associative array given as data
*
* @package DataExplorer\Component\Container\Table
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
*/
class Table extends Div{

    /**
    * @var Tr Header of the table
    */
    private $header;

    /**
    * @var Caption Caption of the table
    */
    private $caption;

    /**
    * Constructor
    *
    * @param array[] $data Data to display
    * @param DataExplorer $de Instance of DataExplorer, to know if the default css is enabled
    * @param string $caption The caption value of the table
    */
    public function __construct(array $data, DataExplorer $de,
                                    string $caption = ''){
        parent::__construct();

        if(count($data) > 0){
            if(\is_array($data[0])){

                // Caption creation
                $this->addNode(new Caption($caption));

                // Header creation
                $this->header = new Tr();
                foreach (\array_keys($data[0]) as  $h) {
                    $th = new Th($h);

                    if($de->default_css_enabled())
                        $th->addClass('alert--inverse');

                    $this->header->addNode($th);
                }

                // Body creation
                foreach ($data as $r) {
                    $row = new Tr();

                    foreach ($r as $k => $v) {
                        $row->addNode(new Td($v));
                    }

                    $this->addNode($row);
                }
            }
        }
    }

    /**
    * Sets new data and re generate the table
    *
    * @param array[] $data Data to display
    * @param DataExplorer $de Instance of DataExplorer, to know if the default css is enabled
    *
    * @return void
    */
    public function setData(array $data, DataExplorer $de) : void {
        if(count($data) > 0){
            if(\is_array($data[0])){
                $this->header->clearNodes();
                // Header creation
                foreach (\array_keys($data[0]) as  $h) {
                    $th = new Th($h);

                    if($de->default_css_enabled())
                        $th->addClass('alert--inverse');

                    $this->header->addNode($th);
                }

                // Body creation
                foreach ($data as $r) {
                    $row = new Tr();

                    foreach ($r as $k => $v) {
                        $row->addNode(new Td($v));
                    }

                    $this->addNode($row);
                }
            }
        }
    }

    public function render() : string{
        $html = "<table id='{$this->id}' class='".\implode(' ',$this->class)."'
            {$this->html_attrs()}>"
                    .PHP_EOL;

        $html .= $this->header->render();

        foreach($this->nodes as $c){
            $html .= $c->render().PHP_EOL;
        }

        $html .= "</table>";

        return $html;
    }
}



 ?>
