<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component\Container;

use DataExplorer\Component\Component;

/**
* Class which represents a html div.
*
* This class is used to model a <code>div</code> (HTML).
*
* @package DataExplorer\Component\Container
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
*/
class Div extends Component {

    public function render() : string{
        $html = "<div id='{$this->id}' class='".\implode(' ',$this->class)."' {$this->html_attrs()}>"
                    .PHP_EOL;

        foreach($this->nodes as $c){
            $html .= $c->render().PHP_EOL;
        }

        $html .= "</div>";

        return $html;
    }
}

 ?>
