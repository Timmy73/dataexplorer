<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component\Text;

/**
* Class which represents the span html tag
*
* This class is used to model the <code>span</code> tag (HTML). In this representation,
* it has been decided that the <code>span</code> tag must contain text and only text.
*
* @package DataExplorer\Component\Text
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
*/
class Span extends P {

    /**
    * Constructor
    *
    * @param string $text Text of span
    */
    public function __construct(string $text){
        parent::__construct($text);
    }

    public function render() : string {
        return "<span  id='{$this->id}' class='".\implode(' ',$this->class)."' {$this->html_attrs()}>
            {$this->txt}</span>";
    }
}


 ?>
