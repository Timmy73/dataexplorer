<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component\Text;

/**
* Class which represents the label html tag
*
* This class is used to model the <code>label</code> tag (HTML). This tag must contain text and only text.
*
* @package DataExplorer\Component\Text
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
*/
class Label extends P{

    /**
    * @var string Value of the <code>for</code> HTML attribute
    */
    private $for;

    /**
    * Constructor
    *
    * @param string $content Label text
    * @param string $for <code>for</code> HTML attribute value, the id of the associated input
    */
    public function __construct(string $content, string $for = ""){
        parent::__construct($content);

        $this->for     = \strtolower($for);
    }

    public function render()  : string {
        return "<label id='{$this->id}' for='{$this->for}'
                    class='".\implode(' ',$this->class)."' {$this->html_attrs()}>".PHP_EOL
                    ."{$this->txt}".PHP_EOL
                ."</label>".PHP_EOL;
    }
}

 ?>
