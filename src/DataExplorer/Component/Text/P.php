<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component\Text;

use DataExplorer\Component\Container\Div;

/**
* Class which represents the p html tag
*
* This class is used to model the <code>p</code> tag (HTML).
* May contain simple text. But can also contain other html components as children.
*
* @package DataExplorer\Component\Text
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
*/
class P extends Div{

    /**
    * @var string Text inside paragraph
    */
    protected $txt;

    /**
    * Constructor
    *
    * @param string $text Text inside paragraph
    */
    public function __construct(string $text = ''){
        parent::__construct();

        $this->txt = $text;
    }

    /**
    * Sets the text inside paragraph
    *
    * @param string $text Text to put inside paragraph
    *
    * @return void
    */
    public function setText(string $text) : void {
        $this->txt = $text;
    }

    /**
    * Gets the text inside paragraph
    *
    * @return string The text inside paragraph
    */
    public function text() : string {
        return $this->txt;
    }

    public function render() : string{
        $html = "<p id='{$this->id}' class='".\implode(' ',$this->class)."' {$this->html_attrs()}>"
                    .PHP_EOL;
        $html .= $this->txt;

        foreach($this->nodes as $c){
            $html .= $c->render().PHP_EOL;
        }

        $html .= "</p>";

        return $html;
    }
}



 ?>
