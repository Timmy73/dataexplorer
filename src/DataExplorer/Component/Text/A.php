<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component\Text;

/**
* Class which represents the a html tag
*
* This class is used to model the <code>a</code> tag (HTML). In this representation,
* it has been decided that the <code>a</code> tag must contain text and only text.
* This component needs an attribute: href
*
* @package DataExplorer\Component\Text
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
*/
class A extends Span {

    /**
    * @var string Url of the link
    */
    private $url;

    /**
    * Constructor
    *
    * @param string $text The link text
    * @param string $url Url of the link
    */
    public function __construct(string $text, string $url){
        parent::__construct($text);

        $this->url = $url;
    }

    public function render() : string {
        return "<a href='{$this->url}' id='{$this->id}'
                    class='".\implode(' ',$this->class)."' {$this->html_attrs()}>
            {$this->txt}</a>";
    }
}

 ?>
