<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer\Component\Text;

/**
* Class which represents the caption html tag
*
* This class is used to model the <code>caption</code> tag (HTML).
* This tag must contain text and only text.
*
* @package DataExplorer\Component\Text
* @category GraphicalComponent
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @used-by DataExplorer\Component\Container\Table\Table
* @version 0.1
*/
class Caption extends Span {

    public function render() : string {
        return "<caption  id='{$this->id}' class='".\implode(' ',$this->class)."' {$this->html_attrs()}>
            {$this->txt}</caption>";
    }
}


 ?>
