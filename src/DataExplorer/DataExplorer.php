<?php
/**
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
* @filesource
*/

namespace DataExplorer;

use DataExplorer\Component\Text\P;
use DataExplorer\Component\Text\A;
use DataExplorer\Component\Text\Label;

use DataExplorer\Component\Container\Div;
use DataExplorer\Component\Container\Table\Table;

use DataExplorer\Component\Form\Form;
use DataExplorer\Component\Form\Input\Input;
use DataExplorer\Component\Form\Input\Email;
use DataExplorer\Component\Form\Input\Number;
use DataExplorer\Component\Form\Input\Submit;

/**
* Main class that will process the data
*
* This class is used to model a <code>div</code> (HTML).
*
* @package DataExplorer
* @category Backend
* @author Thomas FRION <thomas.frion@protonmail.com>
* @copyright 2020 - Thomas FRION
* @version 0.1
*/
class DataExplorer{

    /**
    * @var DataExplorer Instance of <code>DataExplorer</code> class
    */
    private static $instance;

    /**
    * @var string Current page url (with the GET parameters)
    */
    private $url;
    /**
    * @var array Associative array to work with, contains the whole data
    */
    private $data;

    /**
    * @var int Offset of the pagination
    */
    private $offset;
    /**
    * @var array Array which contains filters
    */
    private $filters;
    /**
    * @var int Number of rows per page (default set at 10)
    */
    private $nb_per_page;
    /**
    * @var bool Option to activate the provided CSS, default is set at true
    */
    private $default_css;
    /**
    * @var array The associative array containing the filtered data
    */
    private $filtered_data;


    /**
    * @var DataExplorer\Component\Container\Table\Table Data strucutre to generate table' HTML code
    */
    private $table;
    /**
    * @var DataExplorer\Component\Text\P Data strucutre to generate pagination' HTML code
    */
    private $pagination;
    /**
    * @var DataExplorer\Component\Form\Form Data strucutre to generate form' HTML code (filters)
    */
    private $form;
    /**
    * @var DataExplorer\Component\Container\Div Data strucutre containing the whole (HTML) structure of DataExplorer
    */
    private $main_container;

    /**
    * __construct Constructor
    *
    * @param array $data Data to be processed
    * @param int $nb_per_page Number of rows per page (default set at 10)
    * @param string $form_meth Method used by the form (GET | POST)
    *
    * @return DataExplorer Instance of DataExplorer
    */
    private function __construct(array $data, int $nb_per_page = 10,
                    string $form_meth = 'get'){

        $this->filtered_data = null;
        $this->default_css    = true;

        $this->offset      = isset($_GET['off']) ? $_GET['off'] : 1;
        $this->data        = $data;
        $this->nb_per_page = $nb_per_page;
        $this->url         = $_SERVER['REQUEST_SCHEME'].'://'.
                                $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

        $this->generate_form($form_meth);

    }

    /**
    * instance Get an instance of DataExplorer (i.e. Singleton pattern)
    *
    * @param array $data Data to be processed
    * @param int $pagination Number of rows per page (default set at 10)
    * @param string $form_meth Method used by the form (GET | POST)
    *
    * @return DataExplorer Instance of DataExplorer
    */
    public static function instance(array $data = null, int $pagination = 10,
            string $form_meth = 'get') : DataExplorer {
        if(!(self::$instance instanceof self)){
            if(self::$instance == null && (isset($data) || !empty($data))){
                self::$instance = new self($data, $pagination, $form_meth);
            }
        }

        return self::$instance;
    }

    /**
    * set_default_css Allows to enable/disable the default CSS
    *
    * @param bool $b True to enable, false to disable
    *
    * @return void
    */
    public function set_default_css(bool $b) : void{
        $this->default_css = $b;
    }

    /**
    * default_css_enabled Aims to know if default CSS is enabled
    *
    * @return bool True if the default css is enable, false otherwise
    */
    public function default_css_enabled() : bool {
        return $this->default_css;
    }

    /**
    * set_pagination Set the number of rows per page
    *
    * @param int $nb_per_page The number of rows per page
    *
    * @return void
    */
    public function set_pagination(int $nb_per_page) : void{
        $this->nb_per_page = $nb_per_page;
    }

    /**
    * form Get the generated form
    *
    * @return Form the generated form
    */
    public function form() : Form {
        return $this->form;
    }

    /**
    * structure Gets the entire component structure
    *
    * @return Div The main container with the whole structure inside
    */
    public function structure() : Div {
        return $this->main_container;
    }

    /**
    * generate Generate the HTML code
    *
    * @return string HTML code of the whole structure (form, table, pagination)
    */
    public function generate() : string {

        $this->generate_table();

        if($this->nb_per_page > 0){
            $this->generate_pagination();
        }

        $this->generate_structure();
        $html = "";

        if($this->default_css){
            $this->set_css_classes();
            $html = "<style>".
                \file_get_contents(__DIR__.'/assets/CSS/knacss.css')."</style>";
        }

        $html .= $this->main_container->render();

        return $html;
    }

    /**
    * filter Filters the data with given filters
    *
    * @param array $filters Filters array
    *
    * @return void
    */
    public function filter(array $filters) : void {
        $inputs  = $this->form->inputs();
        $this->filters = \array_intersect_key($filters, $inputs);

        $this->filtered_data =
            \array_filter($this->data, function($elem){
                $return = false;

                foreach($elem as $k => $v){
                    $k = \strtolower($k);
                    if(\is_string($v) && !empty($this->filters[$k])){

                        $haystack = \strtolower($v);
                        $needle   = \strtolower($this->filters[$k]);

                        $return |= (\strpos($haystack, $needle) !== FALSE);

                    }else if(\is_int($v) || \is_float($v)){
                        $return |= ($v == $this->filters[$k]);
                    }
                }

                return $return;
            });

        foreach($this->filters as $field => $v){
            $this->form->input($field)->setOptions(['value' => $v]);
        }
    }

    /**
    * real_offset Returns the real offset, that means the index of data array
    *
    * @return int The real offset
    */
    private function real_offset() : int{
        return ($this->offset - 1) * $this->nb_per_page;
    }

    /**
    * generate_form Generates the form for filter data
    *
    * @param string $form_meth The method to use for the form (GET | POST)
    *
    * @return void
    */
    private function generate_form(string $form_meth) : void {
        $this->form = new Form($form_meth);
        $data       = $this->data[0];

        foreach ($data as $label => $field) {

            if(\filter_var($field, \FILTER_VALIDATE_EMAIL)) {
                $i = new Email($label);
            } else if(\filter_var($field,\FILTER_VALIDATE_INT) ||
                        \filter_var($field,\FILTER_VALIDATE_FLOAT)) {

                $i = new Number($label);
            } else {
                $i = new Input($label);
            }

            $i->setId($label);
            $this->form->addInput($i, $this);
        }

        $this->form->addInput(new Submit("submit","Chercher"), $this);
        $this->form->setReset($this->reset_link(), $this);
    }

    /**
    * generate_table Generates the table containing data
    *
    * @return void
    */
    private function generate_table() : void {
        $display_data = $this->data;
        if(isset($this->filtered_data) && !empty($this->filtered_data))
            $display_data = $this->filtered_data;

        $data_table = ($this->nb_per_page > 0) ?
            \array_slice($display_data, $this->real_offset(), $this->nb_per_page)
            : $display_data;

        $start = $this->real_offset() + 1;
        $end   = $start + count($data_table) - 1;
        $caption = "$start - $end / ".\count($display_data);

        $this->table = new Table($data_table, $this, $caption);
    }

    /**
    * generate_pagination Generates the pagination
    *
    * @return void
    */
    private function generate_pagination() : void {

        $total = (isset($this->filtered_data) && !empty($this->filtered_data)) ?
                count($this->filtered_data) : count($this->data);

        $nb_link = ceil($total / $this->nb_per_page);
        $this->pagination = new P();

        if($this->offset > 1){
            $prev = $this->offset - 1;
            $url  = $this->set_url_parameter($prev);

            $this->pagination->addNode(new A("< Précédent", $url));
        }

        for($i = 1; $i <= $nb_link; $i++){
            $url = $this->set_url_parameter($i);
            $a   = new A($i, $url);

            if($i == $this->offset){
                $a->addClass("active");
            }

            $a->addOption("style", "padding: 0 5px;");
            $this->pagination->addNode($a);
        }

        if($this->offset < $nb_link){
            $next = $this->offset + 1;
            $url  = $this->set_url_parameter($next);

            $this->pagination->addNode(new A("Suivant >", $url));
        }
    }

    /**
    * generate_structure Generates the main strucutre
    *
    * @return void
    */
    private function generate_structure() : void {

        $div_form             = new Div();
        $div_table            = new Div();
        $div_pagin            = new Div();
        $this->main_container = new Div();

        $div_form->addNode($this->form);
        $div_table->addNode($this->table);
        $div_pagin->addNode($this->pagination);


        $this->main_container->addNode($div_form,  'form');
        $this->main_container->addNode($div_table, 'table');
        $this->main_container->addNode($div_pagin, 'pagin');
    }

    /**
    * set_url_parameter Rewrites the current page URL to add or update the pagination's offset
    *
    * @param int $i The offset
    *
    * @return string The new URL
    */
    private function set_url_parameter(int $i) : string {
        $url = "";

        if(isset($_GET) && isset($_GET['off'])){
            unset($_GET['off']);

            $this->url  = \substr($this->url, 0, \strpos($this->url, '?'));
            $this->url .= '?'.\http_build_query($_GET);

            $url = $this->url."&off=$i";

        } else if(isset($_GET) && !empty($_GET) && !isset($_GET['off'])) {
            $url = $this->url."&off=$i";
        } else {
            if(\strpos($this->url,'?') === FALSE){
                $this->url .= '?';
            }
            $url = $this->url."off=$i";
        }

        return $url;
    }

    /**
    * reset_link Generates a link for reseting the filter form
    *
    * @return string The reset link
    */
    private function reset_link() : string {
        $args      = [];
        $link      = $this->url;
        $to_remove = \array_keys($this->form->inputs());
        $to_remove = \array_merge($to_remove, ['submit', 'off']);

        \parse_str(\parse_url($link, \PHP_URL_QUERY),$args);

        foreach($to_remove as $k){
            unset($args[$k]);
        }

        $link = \substr($link, 0, \strpos($link,'?'));
        $args = \http_build_query($args);

        if(!empty($args)){
            $link .= "?$args";
        }

        return $link;
    }

    /**
    * set_css_classes Sets classes for CSS
    *
    * @return void
    */
    private function set_css_classes() : void{
        $this->table->addClass('table');
        $this->form->addClasses(['grid-4','has-gutter']);
        $this->main_container->addClass("flex-container--column");
        $this->main_container->addOption("style", "padding:20px;");
        $this->main_container->getNode('pagin')->addClass("txtcenter");
    }
}


 ?>
