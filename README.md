# DataExplorer

2020 - Thomas FRION <thomas.frion@protonmail.com>

This library allows you to automatically create an HTML table,
the associated pagination and a form of filters from a PHP array
of raw data.

Form fields are created according to the type of each column in the table:

|**Var type**|**HTML input type**|
|:------:|:-------------:|
|  Int   |     Number    |
| Email  |     Email     |
| Other  |     Text      |


**IMPORTANT:** This library uses a default style. This style requires the use of
[knacss](https://www.knacss.com/). The CSS file of knacss is provided with this library


## Getting started

### Installation

To install the library you have to use `composer`:

```
$ composer require tfrion/DataExplorer
```

### Basic usage

```php
require_once __DIR__."/../vendor/autoload.php";

use DataExplorer\DataExplorer;

/**
*   Do something here
*/

// Get your data
$data = ...

// Get an instance of DataExplorer
$de = DataExplorer::instance($data);

// If there are filters, we apply those filters to data
if(isset($_GET['submit'])){
    $de->filter($_GET);
}

// We generate the html code
echo $de->generate();
```

>**NOTE:** other examples are available in `examples` directory
