<?php
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', true);


require_once __DIR__."/../vendor/autoload.php";

use DataExplorer\DataExplorer;

// Get data to test
$res  = json_decode(file_get_contents("db.json"),true)['adherents'];

// Get an instance of DataExplorer
$de = DataExplorer::instance($res);

$de->set_default_css(false); // default CSS now is disabled

// If there are filters, we apply those filters to data
if(isset($_GET['submit'])){
    $de->filter($_GET);
}

// We generate the html code
echo $de->generate();

 ?>
