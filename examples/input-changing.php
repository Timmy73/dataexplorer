<?php
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', true);


require_once __DIR__."/../vendor/autoload.php";

use DataExplorer\DataExplorer;
use DataExplorer\Component\Form\Input\Input;

// Get data to test
$res  = json_decode(file_get_contents("db.json"),true)['adherents'];
//
// Get an instance of DataExplorer
$de = DataExplorer::instance($res);


/*************************************************/
/*  INPUT CHANGING                               */
/*************************************************/

// We create a basic input (type = text)
$new_input = new Input('input_name', 'input_placeholder');

$fieldName = "email"; // By default the field name is column name (raw data array)

// We replace the input for email (type = email) by out new input
$de->form()->setInput($fieldName,$new_input);

/*************************************************/



// If there are filters, we apply those filters to data
if(isset($_GET['submit'])){
    $de->filter($_GET);
}

// We generate the html code
echo $de->generate();

 ?>
